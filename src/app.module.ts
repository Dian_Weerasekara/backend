import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { CompanyModule } from './modules/company/company.module';
import { HeadModule } from './modules/head/head.module';
import { IssuesModule } from './modules/issues/issues.module';
import { ProjectsModule } from './modules/projects/projects.module';


@Module({
  imports: [TypeOrmModule.forRoot(),
    IssuesModule,
    CompanyModule,
    ProjectsModule,
    HeadModule
    
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
