export interface AdminData{
    fname: string;
    lname: string;
    email:string;
    token:string;
}

export interface AdminRo{
    Admin: AdminData;
}