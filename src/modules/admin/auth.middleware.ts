import { HttpException, HttpStatus, Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request } from "express";
import { AdminService } from "./admin.service";
import { SECRET } from '../config';
import * as jwt from 'jsonwebtoken';


@Injectable()
export class AuthAdminMiddleware implements NestMiddleware {
  constructor(private readonly adminService: AdminService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];
      const decoded: any = jwt.verify(token, SECRET);
      const admin = await this.adminService.findById(decoded.id);

      if (!admin) {
        throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
      }

      req.user = admin.Admin;
      next();

    } else {
      throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
    }
  }
}