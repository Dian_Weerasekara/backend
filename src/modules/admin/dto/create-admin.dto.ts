import { IsNotEmpty } from "class-validator";

export class CreateAdminDto {
    
    @IsNotEmpty()
    readonly fname: string;

    @IsNotEmpty()
    readonly lname: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}