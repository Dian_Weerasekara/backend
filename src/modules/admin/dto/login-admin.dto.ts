import { IsNotEmpty } from "class-validator";

export class LoginAdminDto {
    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}