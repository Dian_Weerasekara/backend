import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { AdminEntity } from './admin.entity';
import { LoginAdminDto } from './dto/login-admin.dto';
import * as argon2 from 'argon2';
import { CreateAdminDto } from './dto/create-admin.dto';
import { AdminRo } from './admin.interface';
import { validate } from 'class-validator';
import { SECRET } from '../config';

const jwt = require('jsonwebtoken');

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(AdminEntity)
        private readonly adminRepository: Repository<AdminEntity>,
       ){}

    async findAll(): Promise<AdminEntity[]> {
        return await this.adminRepository.find();
    }

    async findOne({email,password}: LoginAdminDto): Promise<AdminEntity> {
        const admin = await this.adminRepository.findOne({email});

        if(!admin){
            return null;
        }

        if (await argon2.verify(admin.password, password)){
            return admin;
        }
    }

    //create admin
    async create(dto: CreateAdminDto): Promise<AdminRo> {
        const {fname,lname,email, password} = dto;
        const qb = await getRepository(AdminEntity)
        .createQueryBuilder('admin')
        .where('admin.email = :email' , { email });
        

        const admin = await qb.getOne();

        if(admin) {
            const error = {email: 'You entered an existing email, Email must be unique.'};
            throw new HttpException({message: 'Input data validation failed', error}, HttpStatus.BAD_REQUEST);
        }

        //create admin
        let newadmin = new AdminEntity();
        newadmin.fname = fname;
        newadmin.lname = lname;
        newadmin.email = email;
        newadmin.password = password;
        // newCompany.projects = [];

        const error = await validate(newadmin);
        if (error.length > 0) {
            const _errors = {fname: 'Userinput is not valid.'};
            throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);
      
          } 
          else {
            const savedAdmin = await this.adminRepository.save(newadmin);
             return this.buildAdminRO(savedAdmin);
          }
    }

    async findById(id: number): Promise<AdminRo> {
        const admin = await this.adminRepository.findOne(id);

        if(!admin){
            const errors = {Admin: ' not found'};
            throw new HttpException({errors}, 401);
        }

        return this.buildAdminRO(admin);
    }

    private buildAdminRO(admin: AdminEntity) {
        const AdminRo = {
            id: admin.id,
            fname: admin.fname,
            lname: admin.lname,           
            email: admin.email,
            token: this.genarateJWT(admin)
        };

        return {Admin: AdminRo}
    }

    public genarateJWT(admin){
        let today = new Date();
        let exp = new Date(today);
        exp.setDate(today.getDate() + 60);

        return jwt.sign({
            id: admin.id,
            fname: admin.fname,
            lname: admin.lname,
            email: admin.email,
            exp: exp.getTime() / 1000,
        }, SECRET)
    }
}
