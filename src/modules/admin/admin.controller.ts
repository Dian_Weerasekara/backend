import { Body, Controller, HttpException, Post } from '@nestjs/common';
import { AdminRo } from './admin.interface';
import { AdminService } from './admin.service';
import { CreateAdminDto } from './dto/create-admin.dto';
import { LoginAdminDto } from './dto/login-admin.dto';

@Controller('admin')
export class AdminController {
    constructor(private readonly adminService: AdminService){}

    // @Post('/register')
    // async create(@Body() adminData: CreateAdminDto){
    //     return this.adminService.create(adminData);
    // }

    // @Post('/login')
    // async login(@Body() loginAdminDto: LoginAdminDto): Promise<AdminRo> {
    //     const admin = await this.adminService.findOne(loginAdminDto);

    //     const errors = {Admin: 'not found'};
    //     if(!admin) throw new HttpException({errors}, 401);

    //     const token = await this.adminService.genarateJWT(admin);
    //     const {email, fname, lname} = admin;

    //     const Admin = {email, token, fname, lname};
    //     return {Admin};
    // }
}
