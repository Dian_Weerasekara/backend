import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { IssuesEntity } from "./issues.entity";

@Entity('comments_tbl')
export class Comment{

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    body: string;

    @ManyToOne(type => IssuesEntity, issues => issues.comments)
    issues: IssuesEntity;
}