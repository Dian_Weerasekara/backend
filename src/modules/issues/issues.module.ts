import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthCompanyMiddleware } from '../company/auth.middleware';
import { CompanyEntity } from '../company/company.entity';
import { CompanyService } from '../company/company.service';
import { userEntity } from '../company/user.entity';
import { AuthUserMiddleware } from '../company/user.middleware';
import { HeadEntity } from '../head/head.entity';
import { AuthHeadMiddleware } from '../head/head.middleware';
import { HeadService } from '../head/head.service';
import { Comment } from './comments.entity';
import { IssuesController } from './issues.controller';
import { IssuesEntity } from './issues.entity';
import { IssuesService } from './issues.service';

@Module({
    imports:[TypeOrmModule.forFeature([IssuesEntity, Comment, CompanyEntity, HeadEntity, userEntity])],
    providers:[IssuesService,HeadService,CompanyService],
    controllers:[IssuesController]
})
export class IssuesModule {
    public configure(consumer: MiddlewareConsumer){
        consumer
        .apply(AuthHeadMiddleware)
        .forRoutes(
            {path: 'issues',method: RequestMethod.GET},
            {path: 'issues/create',method: RequestMethod.POST},
            {path: 'issues/update/:slug',method: RequestMethod.PUT},
            {path: 'issues/delete/:slug',method: RequestMethod.DELETE},
            {path: 'issues/comment/:slug',method: RequestMethod.POST},
            {path: 'issues/comment/update/:id',method: RequestMethod.PUT},
        );

        consumer
        .apply(AuthCompanyMiddleware)
        .forRoutes(
            {path: 'issues',method: RequestMethod.GET},
            {path: 'issues/create',method: RequestMethod.POST},
            {path: 'issues/update/:slug',method: RequestMethod.PUT},
            {path: 'issues/delete/:slug',method: RequestMethod.DELETE},
            {path: 'issues/comment/:slug',method: RequestMethod.POST},
            {path: 'issues/comment/update/:id',method: RequestMethod.PUT},
        );

        consumer
        .apply(AuthUserMiddleware)
        .forRoutes(
            {path: 'issues',method: RequestMethod.GET},
            {path: 'issues/create',method: RequestMethod.POST},
            {path: 'issues/update/:slug',method: RequestMethod.PUT},
            {path: 'issues/delete/:slug',method: RequestMethod.DELETE},
            {path: 'issues/comment/:slug',method: RequestMethod.POST},
            {path: 'issues/comment/update/:id',method: RequestMethod.PUT},
        )
    }
}
