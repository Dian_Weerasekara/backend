import { IssuesEntity } from "./issues.entity";

interface Comment{
    body: string;
}

interface IssueData{
    slug: string;
    title: string;
    description: string;
    body?: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface CommentsRO {
    comments: Comment[];
}

export interface IssueRO{
    issue: IssuesEntity;
}

export interface IssuesRo{
    issues: IssuesEntity[];
} 