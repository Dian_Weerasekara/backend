export class CreateIssueDto {
    readonly title: string;
    readonly description: string;
    readonly body: string;
    readonly image: string;
}
  