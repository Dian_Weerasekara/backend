import { Body, Controller, Delete, HttpStatus, Param, Post, Put, UploadedFile, UseInterceptors } from '@nestjs/common';
import { Company } from '../company/company.decorators';
import { CreateCommentDto } from './dto/create-comment';
import { CreateIssueDto } from './dto/create-issue.dto';
import { IssuesService } from './issues.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('issues')
export class IssuesController {
    constructor(private readonly issuesService: IssuesService){}

    @Post('/create')
    async createIssue(@Company('id') companyId: number, @Body() data: CreateIssueDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Issue Added Successfully',
            data: await this.issuesService.create(companyId, data)
        };
    }

    @Put('/update/:slug')
    async updateIssue(@Param() params, @Body() issueData: CreateIssueDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Issue updated',
            data: await this.issuesService.update(params.slug, issueData)
        };
    }

    @Delete('/delete/:slug')
    async delete(@Param() params){
        return{
            statusCode: HttpStatus.OK,
            message: 'Issue Deleted!!',
            data: await this.issuesService.delete(params.slug)
        }
    }

    @Post('/comment/:slug')
    async createComment(@Param('slug') slug, @Body() commentData: CreateCommentDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Comment Added!',
            data: await this.issuesService.addComment(slug, commentData)
        };
    }

    @Put('/comment/update/:id')
    async updateComment(@Param('id') id:number, @Body() commentData: CreateCommentDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Comment Updated Successfully!!',
            data: await this.issuesService.updateComment(id,commentData)
        }
    }

    @Post('image')
    @UseInterceptors(
        FileInterceptor("image", {
            dest: "./uploads",
        })
    )
    uploadSingle(@UploadedFile() file) {
        console.log(file)
    }
}
