import { BeforeUpdate, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { CompanyEntity } from "../company/company.entity";
import { Comment } from "./comments.entity";

@Entity('issues_tbl')
export class IssuesEntity{
    
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    slug: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    body: string;

    @Column()
    image: string;

    @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
    created: Date;

    @Column({ type: 'timestamp', default: () => "CURRENT_TIMESTAMP"})
    updated: Date;

    @BeforeUpdate()
    updateTimestamp() {
        this.updated = new Date;
    }

    @OneToMany(type => Comment, comment => comment.issues, {eager: true})
    @JoinColumn()
    comments: Comment[];
    
    @ManyToOne(type => CompanyEntity, company => company.issues)
    company: CompanyEntity;
}