import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CompanyEntity } from '../company/company.entity';
import { Comment } from './comments.entity';
import { CreateCommentDto } from './dto/create-comment';
import { CreateIssueDto } from './dto/create-issue.dto';
import { IssuesEntity } from './issues.entity';
import { CommentsRO, IssueRO } from './issues.interface';

const slug = require('slug');
@Injectable()
export class IssuesService {
    constructor(
        @InjectRepository(IssuesEntity)
        private readonly issueRepository: Repository<IssuesEntity>,
        @InjectRepository(Comment)
        private readonly commentRepository: Repository<Comment>,
        @InjectRepository(CompanyEntity)
        private readonly companyRepository: Repository<CompanyEntity>
    ){}

    //create an issue
    async create(companyId: number,issueData: CreateIssueDto): Promise<IssuesEntity>{
        let issue = new IssuesEntity();

        issue.title = issueData.title;
        issue.description = issueData.description;
        issue.body = issueData.body;
        issue.image = issueData.image;
        issue.slug = this.slugify(issueData.title);
        
        issue.comments = [];

        const newIssue = await this.issueRepository.save(issue);

        const author = await this.companyRepository.findOne({where: {id: companyId}, relations: ['issues']});
        author.issues.push(issue);
        await this.companyRepository.save(author);

        return newIssue;
    }

    //update the issue
    async update(slug: string, issueData: any): Promise<IssueRO>{
        let toUpdate = await this.issueRepository.findOne({slug: slug});
        let updated = Object.assign(toUpdate, issueData);
        const issue = await this.issueRepository.save(updated);
        return{issue};
    }

    //delete the issue
    async delete(slug: string): Promise<DeleteResult>{
        return await this.issueRepository.delete({slug: slug});
    }

    //Add comments
    async addComment(slug: string, commentData): Promise<IssueRO>{
        let issue = await this.issueRepository.findOne({slug});

        const comment = new Comment();
        comment.body = commentData.body;

        issue.comments.push(comment);

        await this.commentRepository.save(comment);
        issue = await this.issueRepository.save(issue);
        return{issue}
    }

    async updateComment(id: number, data:Partial<CreateCommentDto>){
        await this.commentRepository.update({id},data);
        return await this.commentRepository.findOne({ id })
    }

    

    slugify(title: string) {
        return slug(title, {lower: true}) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36)
    }
}
