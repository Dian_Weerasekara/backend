import { BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('project_tbl')
export class ProjectsEntity{
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    created_date: Date;

    @Column()
    deadline: Date;

    @Column()
    slug: string;

    @BeforeUpdate()
    updateTimestamp() {
        this.created_date = new Date;
    }

}