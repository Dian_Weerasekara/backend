import { Body, Controller, Delete, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { CreateCompanyDto } from '../company/dto/create-company.dto';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectsService } from './projects.service';

@Controller('projects')
export class ProjectsController {
    constructor(private readonly projectsService: ProjectsService){}

    @Post('/create')
    async createIssue(@Body() data: CreateProjectDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Issue Added Successfully',
            data: await this.projectsService.create(data)
        };
    }

    @Put('/update/:slug')
    async updateIssue(@Param() params, @Body() projData: CreateCompanyDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Project updated',
            data: await this.projectsService.update(params.slug, projData)
        };
    }

    @Delete('/delete/:slug')
    async delete(@Param() params){
        return this.projectsService.delete(params.slug);
    }

}
