interface ProjectData{
    slug: string;
    title: string;
    description: string;
    deadline: Date;
}

export interface ProjectsRo{
    project: ProjectData;
}