import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthCompanyMiddleware } from '../company/auth.middleware';
import { CompanyEntity } from '../company/company.entity';
import { CompanyService } from '../company/company.service';
import { userEntity } from '../company/user.entity';
import { HeadEntity } from '../head/head.entity';
import { AuthHeadMiddleware } from '../head/head.middleware';
import { HeadService } from '../head/head.service';
import { ProjectsController } from './projects.controller';
import { ProjectsEntity } from './projects.entity';
import { ProjectsService } from './projects.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProjectsEntity, HeadEntity, CompanyEntity, userEntity])],
  controllers: [ProjectsController],
  providers: [ProjectsService, HeadService, CompanyService]
})
export class ProjectsModule {
  public configure(consumer: MiddlewareConsumer){
    consumer
    .apply(AuthHeadMiddleware)
    .forRoutes(
      {path: 'projects',method: RequestMethod.GET},
      {path: 'projects/create',method: RequestMethod.POST},
      {path: 'projects/update/:slug',method: RequestMethod.PUT},
      {path: 'projects/delete/:slug',method: RequestMethod.DELETE},
    );

    consumer
    .apply(AuthCompanyMiddleware)
    .forRoutes(
      {path: 'projects',method: RequestMethod.GET},
      {path: 'projects/create',method: RequestMethod.POST},
      {path: 'projects/update/:slug',method: RequestMethod.PUT},
      {path: 'projects/delete/:slug',method: RequestMethod.DELETE},
    )
  }
}
