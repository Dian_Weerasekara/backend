import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectsRo } from './project.interface';
import { ProjectsEntity } from './projects.entity';

const slug = require('slug');

@Injectable()
export class ProjectsService {
    constructor(
        @InjectRepository(ProjectsEntity)
        private readonly projectsRepository: Repository<ProjectsEntity>
    ){}

    //create a project
    async create(projectData: CreateProjectDto): Promise<ProjectsEntity> {
        let project = new ProjectsEntity();
        project.title = projectData.title;
        project.description = projectData.description;
        project.deadline = projectData.deadline;
        project.slug = this.slugify(projectData.title)

        const newProject = await this.projectsRepository.save(project);

        return newProject;
    }

    // update project
    async update(slug: string, projdata: any): Promise<ProjectsRo> {
        let toUpdate = await this.projectsRepository.findOne({slug: slug});
        let updated = Object.assign(toUpdate,projdata);
        const project = await this.projectsRepository.save(updated);
        return{project}
    }

    async delete(slug: string): Promise<DeleteResult> {
        return await this.projectsRepository.delete({ slug: slug});
      }
    


    slugify(title: string) {
        return slug(title, {lower: true}) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36)
      }
}

