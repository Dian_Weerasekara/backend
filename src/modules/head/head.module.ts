import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HeadController } from './head.controller';
import { HeadEntity } from './head.entity';
import { AuthHeadMiddleware } from './head.middleware';
import { HeadService } from './head.service';

@Module({
  imports:[TypeOrmModule.forFeature([HeadEntity])],
  controllers: [HeadController],
  providers: [HeadService]
})
export class HeadModule {
  public configure(consumer: MiddlewareConsumer){
    consumer
    .apply(AuthHeadMiddleware)
    .forRoutes(
      {path: 'head', method: RequestMethod.GET},
      {path: 'head/register', method: RequestMethod.POST},
      {path: 'head/login', method: RequestMethod.POST}
    );
  }
}
