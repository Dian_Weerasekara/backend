import { Test, TestingModule } from '@nestjs/testing';
import { HeadController } from './head.controller';

describe('HeadController', () => {
  let controller: HeadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HeadController],
    }).compile();

    controller = module.get<HeadController>(HeadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
