import { Test, TestingModule } from '@nestjs/testing';
import { HeadService } from './head.service';

describe('HeadService', () => {
  let service: HeadService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HeadService],
    }).compile();

    service = module.get<HeadService>(HeadService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
