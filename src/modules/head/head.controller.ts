import { Body, Controller, HttpException, Post } from '@nestjs/common';
import { CreateHeadDto } from './dto/create-head.dto';
import { LoginHeadDto } from './dto/login-head.dto';
import { HeadRo } from './head.interface';
import { HeadService } from './head.service';

@Controller('head')
export class HeadController {
    adminService: any;
    constructor(private readonly headService: HeadService){}

    @Post('/register')
    async create(@Body() headData: CreateHeadDto){
        return this.headService.create(headData);
    }

    @Post('/login')
    async login(@Body() loginHeadDto: LoginHeadDto): Promise<HeadRo> {
        const head = await this.headService.findOne(loginHeadDto);

        const errors = {Head: 'not found'};
        if(!head) throw new HttpException({errors}, 401);

        const token = await this.headService.genarateJWT(head);
        const {email, fname, lname} = head;

        const Head = {email, token, fname, lname};
        return {Head};
    }
}
