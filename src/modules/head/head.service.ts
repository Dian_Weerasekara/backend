import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { LoginHeadDto } from './dto/login-head.dto';
import { HeadEntity } from './head.entity';
import * as argon2 from 'argon2';
import { HeadRo } from './head.interface';
import { CreateHeadDto } from './dto/create-head.dto';
import { validate } from 'class-validator';
import { SECRET } from '../config';

const jwt = require('jsonwebtoken')

@Injectable()
export class HeadService {
    constructor(
        @InjectRepository(HeadEntity)
        private readonly headRepository: Repository<HeadEntity>
    ){}

    async findAll(): Promise<HeadEntity[]> {
        return await this.headRepository.find();
    }

    async findOne({email,password}: LoginHeadDto): Promise<HeadEntity> {
        const admin = await this.headRepository.findOne({email});

        if(!admin){
            return null;
        }

        if (await argon2.verify(admin.password, password)){
            return admin;
        }
    }

    //create admin
    async create(dto: CreateHeadDto): Promise<HeadRo> {
        const {fname,lname,email, password} = dto;
        const qb = await getRepository(HeadEntity)
        .createQueryBuilder('head')
        .where('head.email = :email' , { email });
        

        const head = await qb.getOne();

        if(head) {
            const error = {email: 'You entered an existing email, Email must be unique.'};
            throw new HttpException({message: 'Input data validation failed', error}, HttpStatus.BAD_REQUEST);
        }

        //create admin
        let newhead = new HeadEntity();
        newhead.fname = fname;
        newhead.lname = lname;
        newhead.email = email;
        newhead.password = password;
        // newCompany.projects = [];

        const error = await validate(newhead);
        if (error.length > 0) {
            const _errors = {fname: 'Userinput is not valid.'};
            throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);
      
          } 
          else {
            const savedHead = await this.headRepository.save(newhead);
             return this.buildHeadRO(savedHead);
          }
    }

    async findById(id: number): Promise<HeadRo> {
        const head = await this.headRepository.findOne(id);

        if(!head){
            const errors = {Admin: ' not found'};
            throw new HttpException({errors}, 401);
        }

        return this.buildHeadRO(head);
    }

    private buildHeadRO(head: HeadEntity) {
        const HeadRo = {
            id: head.id,
            fname: head.fname,
            lname: head.lname,           
            email: head.email,
            token: this.genarateJWT(head)
        };

        return {Head: HeadRo}
    }

    public genarateJWT(admin){
        let today = new Date();
        let exp = new Date(today);
        exp.setDate(today.getDate() + 60);

        return jwt.sign({
            id: admin.id,
            fname: admin.fname,
            lname: admin.lname,
            email: admin.email,
            exp: exp.getTime() / 1000,
        }, SECRET)
    }
}
