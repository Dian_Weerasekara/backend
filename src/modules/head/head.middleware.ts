import { HttpException, HttpStatus, Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request } from "express";

import { SECRET } from '../config';
import * as jwt from 'jsonwebtoken';
import { HeadService } from "./head.service";


@Injectable()
export class AuthHeadMiddleware implements NestMiddleware {
  constructor(private readonly headService: HeadService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];
      const decoded: any = jwt.verify(token, SECRET);
      const admin = await this.headService.findById(decoded.id);

      if (!admin) {
        throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
      }

      req.user = admin.Head;
      next();

    } else {
      throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
    }
  }
}