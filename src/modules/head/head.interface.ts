export interface HeadData{
    fname: string;
    lname: string;
    email:string;
    token:string;
}

export interface HeadRo{
    Head: HeadData;
}