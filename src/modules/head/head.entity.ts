import { BeforeInsert, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import * as argon2 from 'argon2';

@Entity('head_tbl')
export class HeadEntity{
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    fname: string;

    @Column()
    lname: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @BeforeInsert()
    async hashPassword() {
    this.password = await argon2.hash(this.password);
    }
}