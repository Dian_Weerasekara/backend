import { IsNotEmpty } from "class-validator";

export class LoginHeadDto {
    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}