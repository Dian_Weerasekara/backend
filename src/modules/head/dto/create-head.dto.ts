import { IsNotEmpty } from "class-validator";

export class CreateHeadDto {
    
    @IsNotEmpty()
    readonly fname: string;

    @IsNotEmpty()
    readonly lname: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}