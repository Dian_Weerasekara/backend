import { HttpException, HttpStatus, Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request } from "express";
import { CompanyService } from "./company.service";
import { SECRET } from '../config';
import * as jwt from 'jsonwebtoken';

// @Injectable()
// export class AuthMiddleware implements NestMiddleware {
//     constructor(private readonly companyService: CompanyService){}

//     async use(req: Request, res: Response, next: NextFunction){
//         const authHeaders = req.headers.authorization;
//         if (authHeaders && (authHeaders as string).split(' ')[1]) {
//             const token = (authHeaders as string).split(' ')[1];
//             const decoded: any = jwt.verify(token,SECRET);
//             const comp = await this.companyService.findById(decoded.id);
      
//             if (!comp) {
//               throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
//             }
      
//             req.company = comp.company
//             next();
      
//           } else {
//             throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
//           }
//         }
//     }

@Injectable()
export class AuthCompanyMiddleware implements NestMiddleware {
  constructor(private readonly companyService: CompanyService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];
      const decoded: any = jwt.verify(token, SECRET);
      const user = await this.companyService.findById(decoded.id);

      if (!user) {
        throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
      }

      req.user = user.company;
      next();

    } else {
      throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
    }
  }
}
