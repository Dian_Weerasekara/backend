import { Body, Controller, Delete, HttpException, HttpStatus, Param, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { Company } from './company.decorators';
import { CompanyRo } from './company.interface';
import { CompanyService } from './company.service';
import { CreateCompanyDto } from './dto/create-company.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginCompanyDto } from './dto/login-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('company')
export class CompanyController {
    constructor(private readonly companyService: CompanyService){}

    @Post('/register')
    async create(@Body() compData: CreateCompanyDto){
        return this.companyService.create(compData);
    }

    @Put('/update/company/:id')
    async update(@Param('id') id:number, @Body() companyData: UpdateCompanyDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'Comment Updated Successfully!!',
            data: await this.companyService.update(id,companyData)
        }
    }

    @Post('/login')
    async login(@Body() loginCompanyDto: LoginCompanyDto): Promise<CompanyRo> {
        const comp = await this.companyService.findOne(loginCompanyDto);

        const errors = {Company: 'not found'};
        if(!comp) throw new HttpException({errors}, 401);

        const token = await this.companyService.genarateJWT(comp);
        const {email, company_name, company_type,contact} = comp;
        const company = {email, token, company_name, company_type,contact};
        return {company};
    }

    @Delete('/delete/company/:id')
    async delete(@Param() params){
        return{
            statusCode: HttpStatus.OK,
            message: 'Issue Deleted!!',
            data: await this.companyService.deletecomp(params.id)
        }
    }

    @Post('/register/user')
    async createuser(@Company('id') companyId: number,@Body() data: CreateUserDto){
        return this.companyService.createuser(companyId,data);
    }

    @Post('/create/user')
    async createIssue(@Company('id') companyId: number, @Body() data: CreateUserDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'User Added Successfully',
            data: await this.companyService.createuser(companyId, data)
        };
    }

    @Put('/user/update/:id')
    async updateComment(@Param('id') id:number, @Body() userData: UpdateUserDto){
        return{
            statusCode: HttpStatus.OK,
            message: 'User Updated Successfully!!',
            data: await this.companyService.updateuser(id,userData)
        }
    }

    @Delete('/delete/user/:id')
    async deleteuser(@Param() params){
        return{
            statusCode: HttpStatus.OK,
            message: 'Usere Deleted!!',
            data: await this.companyService.deleteuser(params.id)
        }
    }
}
