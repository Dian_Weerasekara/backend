import { BeforeInsert, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { CompanyEntity } from "./company.entity";
import * as argon2 from 'argon2';

@Entity('user_tbl')
export class userEntity{
    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    username: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @BeforeInsert()
    async hashPassword() {
    this.password = await argon2.hash(this.password);
    }


    @ManyToOne(type => CompanyEntity, company => company.users)
    company: CompanyEntity;
}