import { MiddlewareConsumer, Module, Req, RequestMethod } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyEntity } from './company.entity';
import { userEntity } from './user.entity';
import { AuthCompanyMiddleware } from './auth.middleware';
import { AuthHeadMiddleware } from '../head/head.middleware';
import { HeadService } from '../head/head.service';
import { HeadEntity } from '../head/head.entity';
import { AuthUserMiddleware } from './user.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([CompanyEntity, userEntity, HeadEntity])],
  providers: [CompanyService,HeadService],
  controllers: [CompanyController]
})
export class CompanyModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthHeadMiddleware)
      .forRoutes(
        {path: 'company', method: RequestMethod.GET},
        {path:'company/update/company/:id', method: RequestMethod.PUT},
        {path: 'company/delete/company/:id', method: RequestMethod.DELETE},
        {path: 'company/register',method: RequestMethod.POST},
        {path: 'company/login', method: RequestMethod.POST},
        {path: 'company/register/user', method: RequestMethod.POST},
        {path: 'company/create/user', method: RequestMethod.POST},
        {path: 'company/company/user/update/:id', method: RequestMethod.PUT},
        {path: 'company/delete/user/:id', method: RequestMethod.DELETE}        
      );
    consumer
        .apply(AuthUserMiddleware)
        .forRoutes(
          {path: 'company/create/user', method: RequestMethod.POST },
          {path: 'company/user/update/:id',method: RequestMethod.PUT},
          {path: 'company/delete/user/:id',method: RequestMethod.DELETE}
        );
    consumer
        .apply(AuthCompanyMiddleware)
        .forRoutes(
          {path: 'company', method: RequestMethod.GET},
          {path: 'company/register',method: RequestMethod.POST},
          {path: 'company/update/company/:id',method: RequestMethod.PUT},
          {path: 'api/company/login',method: RequestMethod.POST},
          {path: 'company/register/user',method: RequestMethod.POST},
          {path: 'company/user/update/:id',method: RequestMethod.PUT},
          {path: '/company/delete/user/:id',method: RequestMethod.DELETE}
        )
  }
}
