import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as argon2 from 'argon2';
import { validate } from 'class-validator';
import { DeleteResult, getRepository, Repository } from 'typeorm';
import { CompanyEntity } from './company.entity';
import { CompanyRo, UserRo } from './company.interface';
import { CreateCompanyDto } from './dto/create-company.dto';
import { LoginCompanyDto } from './dto/login-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { SECRET } from '../config';
import { CreateUserDto } from './dto/create-user.dto';
import { userEntity } from './user.entity';
import { UpdateUserDto } from './dto/update-user.dto';

const jwt = require('jsonwebtoken')

@Injectable()
export class CompanyService {
    constructor(
        @InjectRepository(CompanyEntity)
        private readonly copmanyRepository: Repository<CompanyEntity>,
        @InjectRepository(userEntity)
        private readonly userRepository: Repository<userEntity>
    ){}

    async findAll(): Promise<CompanyEntity[]> {
        return await this.copmanyRepository.find();
    }

    async findOne({email,password}: LoginCompanyDto): Promise<CompanyEntity> {
        const company = await this.copmanyRepository.findOne({email});

        if(!company){
            return null;
        }

        if (await argon2.verify(company.password, password)){
            return company;
        }
    }

    async create(dto: CreateCompanyDto): Promise<CompanyRo> {
        const {company_name, email, password} = dto;
        const qb = await getRepository(CompanyEntity)
        .createQueryBuilder('company')
        .where('company.company_name = :company_name' , { company_name })
        .orWhere('company.email = :email', { email });

        const company = await qb.getOne();

        if(company) {
            const error = {company_name: 'Company Name and the email must be unique.'};
            throw new HttpException({message: 'Input data validation failed', error}, HttpStatus.BAD_REQUEST);
        }

        //create company
        let newCompany = new CompanyEntity();
        newCompany.company_name = company_name;
        newCompany.email = email;
        newCompany.password = password;
        // newCompany.projects = [];

        const error = await validate(newCompany);
        if (error.length > 0) {
            const _errors = {company_name: 'Userinput is not valid.'};
            throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);
      
          } 
          else {
            const savedCompany = await this.copmanyRepository.save(newCompany);
             return this.buildCompanyRO(savedCompany);
          }
    }
    
    //update company
    async update(id: number, dto: UpdateCompanyDto): Promise<CompanyEntity> {
        let toUpdate = await this.copmanyRepository.findOne(id);
        delete toUpdate.password;

        let updated = Object.assign(toUpdate, dto);
        return await this.copmanyRepository.save(updated);
    }

    async findById(id: number): Promise<CompanyRo> {
        const comp = await this.copmanyRepository.findOne(id);

        if(!comp){
            const errors = {User: ' not found'};
            throw new HttpException({errors}, 401);
        }

        return this.buildCompanyRO(comp);
    }

    //delete company
    async deletecomp(id: number): Promise<DeleteResult>{
        return await this.copmanyRepository.delete({id});
    }

    //create user
    async createuser(companyId: number, dto: CreateUserDto): Promise<UserRo> {
        const {username, email, password} = dto;
        const qb = await getRepository(userEntity)
        .createQueryBuilder('user')
        .where('user.username = :username' , { username })
        .orWhere('user.email = :email', { email });

        const user = await qb.getOne();

        if(user) {
            const error = {username: 'User Name and the email must be unique.'};
            throw new HttpException({message: 'Input data validation failed', error}, HttpStatus.BAD_REQUEST);
        }

        //create newuser
        let newUser = new userEntity();
        newUser.username = username;
        newUser.email = email;
        newUser.password = password;
        // newCompany.projects = [];

        const error = await validate(newUser);

        // unknown thing testing to pass the id of the company to user as foreing key
                const company = await this.copmanyRepository.findOne({where: {id: companyId}, relations: ['users']});
                company.users.push(newUser);
                await this.copmanyRepository.save(company);
            // unknown thing testing end
        
        if (error.length > 0) {
            const _errors = {company_name: 'Userinput is not valid.'};
            throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);
      
          } 
          else {
              
            const savedUser = await this.userRepository.save(newUser);
             return this.buildUserRO(savedUser);
          }
    }

    async findByIdUser(id: number): Promise<UserRo> {
        const user = await this.userRepository.findOne(id);

        if(!user){
            const errors = {User: ' not found'};
            throw new HttpException({errors}, 401);
        }

        return this.buildUserRO(user);
    }

    //delete user
    async deleteuser(id: number): Promise<DeleteResult>{
        return await this.userRepository.delete({id});
    }

    //update user
    async updateuser(id: number, dto: UpdateUserDto): Promise<userEntity> {
        let toUpdate = await this.userRepository.findOne(id);
        delete toUpdate.password;

        let updated = Object.assign(toUpdate, dto);
        return await this.userRepository.save(updated);
    }

    private buildUserRO(user: userEntity){
        const UserRo = {
            id: user.id,
            username: user.username,
            email: user.email,
            // password: user.password,
            token: this.genarateJWT(user)
        }
        return{user: UserRo}
    }
    
    
    private buildCompanyRO(company: CompanyEntity) {
        const CompanyRo = {
            id: company.id,
            company_name: company.company_name,
            company_type: company.company_type,
            contact: company.contact,
            email: company.email,
            token: this.genarateJWT(company)
        };

        return {company: CompanyRo}
    }

    public genarateJWT(company){
        let today = new Date();
        let exp = new Date(today);
        exp.setDate(today.getDate() + 60);

        return jwt.sign({
            id: company.id,
            company_name: company.company_name,
            email: company.email,
            exp: exp.getTime() / 1000,
        }, SECRET)
    }
}
