export interface CompanyData {
    company_name: string;
    company_type: string;
    contact: string;
    email: string;
    token: string;
}

export interface CompanyRo{
    company: CompanyData;
}

export interface UserData{
    username: string;
    email: string;
    token:string;
}

export interface UserRo{
    user: UserData;
}