import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import * as argon2 from 'argon2';
import { IssuesEntity } from "../issues/issues.entity";
import { userEntity } from "./user.entity";

@Entity('company_tbl')
export class CompanyEntity{

    @PrimaryGeneratedColumn('uuid')
    id: number;

    // @Column()
    // slug:string;

    @Column()
    company_name:string;

    @Column()
    company_type:string;

    @Column()
    contact:string;

    @Column()
    email:string;

    @Column()
    password:string;

    @BeforeInsert()
    async hashPassword() {
    this.password = await argon2.hash(this.password);
    }

    @OneToMany(type => IssuesEntity, issue => issue.company)
    issues: IssuesEntity[];  

    @OneToMany(type => userEntity, user =>user.company)
    users: userEntity[];
}