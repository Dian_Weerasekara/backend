export class UpdateCompanyDto {
    readonly company_name: string;
    readonly company_type: string;
    readonly contact: string;
    readonly image: string;
}