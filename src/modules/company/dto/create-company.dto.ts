import { IsNotEmpty } from "class-validator";

export class CreateCompanyDto {
    @IsNotEmpty()
    readonly company_name: string;

    @IsNotEmpty()
    readonly email: string;

    @IsNotEmpty()
    readonly password: string;
}